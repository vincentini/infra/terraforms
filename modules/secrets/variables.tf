variable "environment" {
  type = string
}

variable "secret_configuration_db_json" {
  type = map(string)
}