resource "aws_secretsmanager_secret" "secret_configuration_db" {
  name = "configuration_db_${var.environment}"

  tags = {
    environment = var.environment
  }
}

resource "aws_secretsmanager_secret_version" "secret_configuration_db_version" {
  secret_id = aws_secretsmanager_secret.secret_configuration_db.id
  secret_string = jsonencode(var.secret_configuration_db_json)
}