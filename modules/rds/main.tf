resource "aws_db_subnet_group" "default" {
  name       = "main"
  subnet_ids = var.subnet_ids

  tags = {
    environment = var.environment
  }
}

// resource "aws_db_security_group" "default" {
//   name = "rds_sg"
//   security_group_id = 

//   ingress {
//     cidr = "0.0.0.0/0"
//   }

//   tags = {
//     environment = var.environment
//   }
// }

resource "aws_db_instance" "configuration_db" {
  allocated_storage    = 20
  storage_type         = "gp2"
  engine               = "postgres"
  engine_version       = "12.2"
  instance_class       = "db.t2.micro"
  name                 = var.configuration_db_name
  username             = var.configuration_db_username
  password             = var.configuration_db_password
  db_subnet_group_name = aws_db_subnet_group.default.name
  publicly_accessible  = var.rds_public

  tags = {
    environment = var.environment
  }
}