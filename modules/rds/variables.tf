variable "configuration_db_username" {
  type = string
}

variable "configuration_db_password" {
  type = string
}

variable "configuration_db_name" {
  type = string
}

variable "environment" {
  type = string
}

variable "subnet_ids" {
  type = list(string)
}

variable "rds_public" {
  type = bool
}