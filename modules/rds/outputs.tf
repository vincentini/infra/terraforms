output "configuration_db_address" {
  value = aws_db_instance.configuration_db.address
}

output "configuration_db_port" {
  value = aws_db_instance.configuration_db.port
}

output "configuration_db_username" {
  value = aws_db_instance.configuration_db.username
}

output "configuration_db_password" {
  value = aws_db_instance.configuration_db.password
}

output "configuration_db_name" {
  value = aws_db_instance.configuration_db.name
}