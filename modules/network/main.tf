resource "aws_security_group" "vicentini_security_group" {
  name = "allow_external_access"
  description = "Allow inbound traffic"
  vpc_id = aws_vpc.vicentini_private_vpc.id

  ingress {
    description = "Abrindo porta do postgresql"
    from_port = 5432
    to_port = 5432
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
resource "aws_vpc" "vicentini_private_vpc" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true
}

resource "aws_internet_gateway" "vicentini_internet_gateway" {
  vpc_id = aws_vpc.vicentini_private_vpc.id
}

data "aws_availability_zones" "available" {}

resource "aws_subnet" "vicentini-subnet" {
  count = 3
  vpc_id                  = aws_vpc.vicentini_private_vpc.id
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  cidr_block              = cidrsubnet(aws_vpc.vicentini_private_vpc.cidr_block, 8, count.index)
  map_public_ip_on_launch = false
}

resource "aws_route_table" "vicentini_route_table" {
  vpc_id = aws_vpc.vicentini_private_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.vicentini_internet_gateway.id
  }
}

resource "aws_route_table_association" "vicentini_route_table_associacion" {
  subnet_id      = aws_subnet.vicentini-subnet.*.id[count.index]
  route_table_id = aws_route_table.vicentini_route_table.id
  count          = 3
}
