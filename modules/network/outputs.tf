output "vpc_id" {
  value = aws_vpc.vicentini_private_vpc.id
}

output "subnet_ids" {
  value = aws_subnet.vicentini-subnet.*.id
}