terraform {
  required_version = "0.12.20"
  
  backend "s3" {
    region="sa-east-1"
    profile="personalerp"
    bucket="vicentini-aws-admin"
    key="vicentini-infra-state-dev"
  }
}

provider "aws" {
  region  = "sa-east-1"
  profile = "personalerp"
  version = "~> 2.52"
}

module "lambda-roles" {
  source = "../modules/lamda-roles"
  account_id = ""
  environment = basename(path.cwd)
}

module "network" {
  source = "../modules/network"
}

module "databases" {
  source = "../modules/rds"
  configuration_db_username = "usr_configuration"
  configuration_db_password = "2zk4TGUF9Bdj5Cd6bJbRZDWnxxFzTA6VLKKWPs2qMwkjKCZPT4d58FThZPusTrpA"
  configuration_db_name = "configdbdev"
  subnet_ids = module.network.subnet_ids
  rds_public = true
  environment = basename(path.cwd)
}

module "secrets" {
  source = "../modules/secrets"
  
  secret_configuration_db_json = {
    db_username = module.databases.configuration_db_username,
    db_password = module.databases.configuration_db_password,
    db_name = module.databases.configuration_db_name,
    db_address = module.databases.configuration_db_address,
    db_port = module.databases.configuration_db_port
  }

  environment = basename(path.cwd)
}